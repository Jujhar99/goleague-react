﻿﻿var React = require('react');
var ReactDOM = require('react-dom');
import { createStore } from 'redux'

const initialState = {
    login: 0,
    list: [{'name':'Get screener done', 'date': '10/20/2012', 'status':1},
        {'name':'gardening', 'date': '10/20/2012', 'status':0},
        {'name':'emails', 'date': '10/20/2012', 'status':0},
    ]
}

// Redux
const counter = (state = initialState, action) => {
    let stateUpdate = state;
    switch (action.type) {
        case 'LOGIN':
            console.log("login attempt:" + action.login)
            if (action.login == 'demo' && action.pw == 'demo') {
                stateUpdate.login = 1;
            }
            return stateUpdate;

        // Start with no items (from Start fresh! click)
        case 'LOGIN-NEW':
            console.log("login attempt:" + action.login)
            if (action.login == 'demo' && action.pw == 'demo') {
                stateUpdate.list = [];
                stateUpdate.login = 1;
            }
            return stateUpdate;

        case 'ADD_TODO':
            stateUpdate.list.push({'name':action.text,'date': action.date, 'status':action.status});
            console.log(stateUpdate);
            return stateUpdate;

        case 'UPDATE_TODO_CHECKBOX':
            stateUpdate.list[action.position].status = action.status;
            console.log(stateUpdate);
            return stateUpdate;

        case 'UPDATE_TODO':
            stateUpdate.list[action.position].date = action.date;
            console.log(stateUpdate);
            return stateUpdate;

        case 'DELETE_TODO':

            // items with empty name will be filtered out of task list
            stateUpdate.list[action.position].name = '';
            return stateUpdate;
        default:
            return state;
    }

}

const store = createStore(counter)


class HelloWorld extends React.Component {

constructor(props) {
        super(props);
        this.state = this.props.todos;
    }

    // Sort and highlight list using jQuery (index.html)
    componentDidMount() {
        ordertodo();
    }

    // Sort and highlight list using jQuery (index.html)
    componentDidUpdate() {
        ordertodo();
        initdatepicker();
    }


    render(){

        // login
        if (this.props.todos.login == 0){
            console.log("not logged in");

            return (
                <div><a style={{textDecoration:"none"}} href="index.html"><h1>GOLeague</h1></a>

                    <div>
                    <h2 id="login-h">Please login</h2>

                    Username<br />
                    <input id="login" defaultValue="demo" /><br />

                    Password<br />
                    <input id="pw" type="password" value="demo"/><br />

                    <br /><br />
                    <button onClick={() => {
                        $( "#login" ).delay(100).css("background-color","salmon" );
                        $( "#pw" ).delay(100).css("background-color","salmon" );
                        $( "#loginmsg" ).html("login is invalid");
                        store.dispatch({
                            type: 'LOGIN',
                            login: login.value,
                            pw: pw.value
                            });
                        }
                        } >Go</button><br />

                    <button style={{
                            marginTop:"-28px",
                            marginLeft:"140px",
                            color:"white",
                            backgroundColor:"green"}}
                    onClick={() => {
                        $( "#login" ).delay(100).css("background-color","salmon" );
                        $( "#pw" ).delay(100).css("background-color","salmon" );
                        $( "#loginmsg" ).html("login is invalid");
                        store.dispatch({
                            type: 'LOGIN-NEW',
                            login: login.value,
                            pw: pw.value
                            });
                        }}>
                        Start fresh!
                    </button>
                    </div>

                    <br /><br /><div id="loginmsg" style={{color:"#333"}} ></div>
                </div>
            )

        }
        // End of login

        // Loop through to do items and add them to variable
        let position = 0;
        var toDoList = this.props.todos.list.map(function(name){

            // deleted items are empty
            if (name.name == '') {
                return <Item cname="nodisplay" value='derp' date={name.date} status={name.status} index={position++}/>;
            }

            else if (name.name != '') {
                return <Item cname="item"  value={name.name} date={name.date} status={name.status} index={position++}/>;
            }
        })

        return (
            <div><a style={{textDecoration:"none"}} href="index.html"><h1>GOLeague</h1></a>

                <table id="table">
                { toDoList }
                </table>

                <br />

                <form id="newtask" onsubmit="return validateForm();">
                    <div id="controls" draggable="true">
                        New Task<br />
                        <input id="newTask" name="newTask" /><br />

                        Due date<br />
                        <input id="datepicker" /><br />

                        Status<br />
                        <select id="newStatus">
                            <option value="0">Pending</option>
                            <option value="1">Done</option>
                        </select>

                        <input type="submit" id="add-button" value="Create new Item" onClick={(e) => {
                        e.preventDefault();

                        if (datepicker.value == ''){

                            // if no date is selected
                            // display today in MM/DD/YYYY format
                            var today = new Date();
                            var todayDay = ("0" + (today.getDay() + 1)).slice(-2);
                            var todayMonth = ("0" + (today.getMonth()+1)).slice(-2);
                            datepicker.value =  todayMonth + "/" + todayDay + "/" + today.getFullYear();
                        };

                        store.dispatch({
                            type: 'ADD_TODO',
                            text: newTask.value,
                            date: datepicker.value,
                            status: newStatus.value,
                        });

                        // clean up
                        datepicker.value = '';

                        }}/>
                    </div>
                </form>

            </div>
    );}}

// Each todo item
class Item extends React.Component {

    render(){
        return (
                <tr id={this.props.index} className={this.props.cname}>
                    <td>
                        <input type="checkbox" className="squaredOne" value="1" defaultChecked={parseInt(this.props.status)} onClick={(event, i=33) => {

                        $("#"+ this.props.index +" td .field").prop('disabled', event.target.checked);

                                // remove class
                                $("#"
                                +(this.props.index+1)+" td").removeClass('oldtask');

                                store.dispatch({
                                type: 'UPDATE_TODO_CHECKBOX',
                                position: this.props.index,
                                status: event.target.value
                                });
                                }
                        }/>
                    </td>
                    <td>
                <input id="namedInput" type="text" name="name" class="field" onChange={() => (this.setState({name:(this.textInput)}))}
                defaultValue={this.props.value} disabled={parseInt(this.props.status)} /><br />
                        </td>
                    <td className="click" onClick={(event) => {
                     var datenew = prompt("choose new date:", this.props.date);

                     if (datenew.length == 10){
                     store.dispatch({
                        type: 'UPDATE_TODO',
                        position: this.props.index,
                        date: datenew,
                     });
                     };
                     }}>
                        {this.props.date}
                    </td>
                    <td>
                        <div className="block">
                        <button id="deletebtn" onClick={(event) => {

                        var result = confirm("You are about to delete '"+this.props.value+"'.");
                        if (result) {
                            store.dispatch({
                            type: 'DELETE_TODO',
                            position: this.props.index,
                            });
                            }
                        }
                        }>Delete</button>
                        </div>
                    </td>
                </tr>

);}}

const render = () => {
    console.log(store.getState())
    ReactDOM.render(<HelloWorld todos={store.getState()}/>, document.getElementById("react"));
};

console.log(store.getState())
store.subscribe(render);
render();
